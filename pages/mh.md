# Monster Hunter

![mh_logo](mh_logo.png)

## Quick description

Collection of resources for the Monster Hunter room

## Contents

- [General links](#general-links)
- [Basic stuff](#basic-stuff)
- [Advanced stuff](#advanced-stuff)

## General links

- [MHW PC Schedule](http://game.capcom.com/world/steam/us/schedule.html?utc=1)
- [Kiranico MHW Database](https://mhworld.kiranico.com/)
- [/mhg/ Institution of Higher Learning](https://pastebin.com/FLUWTZdX)

## Basic stuff

- [MHW PC/Newbie FAQ](https://pastebin.com/cHJzcbEs)
- [Reddit New Player Guide](https://www.reddit.com/r/MonsterHunter/wiki/new_player_guides)
- [Reddit Useful Applications](https://www.reddit.com/r/MonsterHunter/wiki/useful_application_listing)
- [Kiranico MHW Guide](https://mhworld.kiranico.com/guide/introduction)

## Advanced stuff

- [Reddit Visual Tempered Threat List](https://www.reddit.com/r/MonsterHunter/comments/7xryte/had_a_few_minutes_so_made_a_visual_tempered/)
- [MHW Arekkz Weapon Workshop](https://www.youtube.com/playlist?list=PLHc2Wj95htvMxZR7dvgYwevupNBy9imiu)
- [MHW Gaijin Hunter Weapon Tutorials](https://www.youtube.com/playlist?list=PLDR3ta7VqKrwkrYeHyqSKcFEcJSCWZ26L)
- [MHW Arekkz Monster Guides](https://www.youtube.com/playlist?list=PLHc2Wj95htvPiD5FNwjjilpF9O2D55AxD)
- [MHW Arekkz Investigation series](https://www.youtube.com/playlist?list=PLHc2Wj95htvMtNnOQ2r1xFfSBrrzUGAyF)
- [A guide to MH World's Endgame](https://www.reddit.com/r/MonsterHunterWorld/comments/a0epri/guide_ive_just_beat_xenojiva_what_do_a_guide_to/)
