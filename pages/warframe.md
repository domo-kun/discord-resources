# Warframe

![warframe_logo](warframe_logo.jpg)

```text
WRYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY
```

## Quick description

Collection of resources for the Waframe room

## Contents

- [General links](#general-links)
- [Basic stuff](#basic-stuff)
- [Advanced stuff](#advanced-stuff)
- [POE stuff](#poe-stuff)
- [Fortuna stuff](#fortuna-stuff)
- [Nerd Stats](#nerd-stats)

## General links

- [Wiki](http://warframe.wikia.com/)
- [Warframe Hub](https://hub.warframestat.us/)
- [DeathSnacks Tracker](http://deathsnacks.com/wf/index.html)
- [Warframe Market](https://warframe.market/)
- [Warframe News](https://semlar.com/)
- [Prime parts Locator](https://www.war.farm/index.php)

## Basic stuff

- [GENERAL FAQ](http://pastebin.com/wyz3Ye1g)
- [Additional FAQ, SERIOUSLY, READ THIS ONE](http://pastebin.com/GHStmm22)
- [Top Guns](https://docs.google.com/spreadsheets/d/1mdgJUyNrsHWRMveKwFrpdMJQWyyEGE4Wpo34e8BzE74)
- [Frames and Modding](https://pastebin.com/D8Ckdz0c)
- [Reddit Handbook](https://docs.google.com/document/d/1vR41ia5_FmKfyfL9oGakbTc1qkzDJuL3AQ7-F40gmW4/)

## Advanced stuff

- [DO "How to Build" Youtube Playlist](https://www.youtube.com/watch?v=y-1ZVMaZX_o&list=PLi8xLqHWyQOrNpqNywCiouYp7EMi3l5xP)
- [Mandachord Requests WF Forum](https://forums.warframe.com/topic/786389-1955-mandachord-requests-fulfilled-back-august-8th/)

## POE stuff

- [Fishing 101](https://forums.warframe.com/topic/858467-fishing-101-active-updating/)
- [Fishing Video Guide](https://www.youtube.com/watch?v=AuSZz0vtkYI&feature=youtu.be)
- [Basic Teralyst Guide](https://docs.google.com/document/d/188nr-Q0lK4_Uj5w_btWMxAWq6iaNuQytvJU9nFC07eY/edit)
- [Eidolong Hunting 101](https://padlet.com/alchameth/EidolonHunting)
- [Reddit Guide: How to Kill an Eidolon](https://www.reddit.com/comments/762sjw)
- [Eidolon Equipment Guide 2.0](http://warframe.wikia.com/wiki/User_blog:EXN0V4/Eidolon_Equipment_Guide_2.0)
- [Eidolon Hunting reddit tips](https://www.reddit.com/r/Warframe/comments/8nquz7/standard_tactics_for_tridolon_hunts_lure/)
- [Lure Locations](https://imgur.com/jm03STZ)
- [Trinity Lure Route](https://i.imgur.com/nn7t9br.png)
- [Advanced Hydrolyst Hunting Guide](https://docs.google.com/document/d/1NLDtt7AhPx3q2WKQmVAiy_RdR_nHZLUgmP0paQ2z54A/edit)
- [Warframe Raid Schoolbus Discord](https://discord.gg/aUTTyWH)
- [Warframe Exploration Hub Discord](https://discordapp.com/invite/ytRaRq4)
- [In Depth 5x3 Cap Guide by Gluttony of the Lotus and -Bliekord-](https://docs.google.com/document/d/1eY6ZZUbLoZwFDmf1WgdOMnM_bjG8ooqV_osHCCxu3Yw/edit)

## Fortuna stuff

- [Frame Mastery Ultimate Fortuna Guide](https://www.framemastery.com/the-ultimate-fortuna-guide/)
- [Fishing map Guide](https://vignette.wikia.nocookie.net/warframe/images/6/6f/FortunaFishingMap.jpg/revision/latest?cb=20181113071342)

## Nerd Stats

- [Warframe Builder](http://warframe-builder.com)
- [Droptables by DE (probably rigged)](http://www.warframe.com/repos/hnfvc0o3jnfvc873njb03enrf56.html)

## ~~Raids (RIP)~~

- [THE LAW OF RETRIBUTION](http://pastebin.com/dMMdZRpY)
- [THE JORDAS VERDICT](http://pastebin.com/NLPSMBed)
- [Warframe Raid Schoolbus Discord](https://discord.gg/aUTTyWH)
